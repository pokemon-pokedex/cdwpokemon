# github.com/asaskevich/govalidator v0.0.0-20210307081110-f21760c49a8d
## explicit
# github.com/davecgh/go-spew v1.1.1
github.com/davecgh/go-spew/spew
# github.com/go-chi/chi v4.1.2+incompatible
## explicit
github.com/go-chi/chi
github.com/go-chi/chi/middleware
# github.com/gorilla/mux v1.8.0
## explicit
github.com/gorilla/mux
# github.com/igknot/chi-zap-ecs-logger v0.0.0-20210521132916-301d52e01749
## explicit
github.com/igknot/chi-zap-ecs-logger
# github.com/magefile/mage v1.9.0
github.com/magefile/mage/mg
# github.com/nu7hatch/gouuid v0.0.0-20131221200532-179d4d0c4d8d
## explicit
github.com/nu7hatch/gouuid
# github.com/pkg/errors v0.9.1
github.com/pkg/errors
# github.com/pmezard/go-difflib v1.0.0
github.com/pmezard/go-difflib/difflib
# github.com/rs/cors v1.9.0
## explicit
github.com/rs/cors
# github.com/stretchr/objx v0.1.0
github.com/stretchr/objx
# github.com/stretchr/testify v1.7.0
## explicit
github.com/stretchr/testify/assert
github.com/stretchr/testify/mock
# go.elastic.co/ecszap v1.0.0
## explicit
go.elastic.co/ecszap
go.elastic.co/ecszap/internal
# go.uber.org/atomic v1.7.0
go.uber.org/atomic
# go.uber.org/multierr v1.6.0
go.uber.org/multierr
# go.uber.org/zap v1.17.0
## explicit
go.uber.org/zap
go.uber.org/zap/buffer
go.uber.org/zap/internal/bufferpool
go.uber.org/zap/internal/color
go.uber.org/zap/internal/exit
go.uber.org/zap/zapcore
# gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b
gopkg.in/yaml.v3
