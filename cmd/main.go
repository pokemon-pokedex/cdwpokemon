package main

import (
	"github.com/rs/cors"
	"gitlab.standartdbank.co.za/cadi/cdwPokemon/pkg/router"
	"log"
	"net/http"
)

func main() {
	log.Println("Starting the application...")
	router := router.Router()
	corsOpts := cors.New(cors.Options{
		AllowedOrigins: []string{"http://localhost:4200"},
		AllowedMethods: []string{
			http.MethodPost,
			http.MethodGet,
		},

		AllowedHeaders: []string{
			"X-Session-Token",
		},
		ExposedHeaders: []string{
			"X-Session-Token",
		},
	})
	log.Println("Service active and listening on port 8080")
	log.Fatal(http.ListenAndServe(":8080", corsOpts.Handler(router)))
}
