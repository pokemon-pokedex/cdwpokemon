FROM golang AS buildStage
RUN go version

ENV GOBIN /go/bin
ENV GOPATH /go/
ENV GOOS linux
ENV GOARCH amd64
ENV CGO_ENABLED 0
ENV GOPROXY 'https://tools.standardbank.co.za/nexus/repository/qltrade-go-modules/'


WORKDIR /thisdir
COPY . .
COPY cacert.pem /etc/ssl/certs
RUN go get ./...
RUN CGO_ENABLED=0 go build  -o /go/bin/ ./...

#RUN ls -ltr /go/bin

FROM alpine

WORKDIR /app
COPY --from=buildStage /go/bin/* /app/myapp
COPY info.json ./info.json
COPY swagger.json ./swagger.json

RUN mkdir -p ./pkg/auth
COPY pkg/auth/* ./pkg/auth/


EXPOSE 8080 8080
RUN adduser -D myuser
USER myuser
ENTRYPOINT ["/app/myapp"]

