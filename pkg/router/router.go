package router

import (
	"fmt"
	"github.com/gorilla/mux"
	czel "github.com/igknot/chi-zap-ecs-logger"
	"gitlab.standartdbank.co.za/cadi/cdwPokemon/pkg/handler"
	"gitlab.standartdbank.co.za/cadi/cdwPokemon/pkg/middleware"
	"gitlab.standartdbank.co.za/cadi/cdwPokemon/pkg/service/misc"

	"net/http"
)

func Router() *mux.Router {
	logger := misc.InitLogger()
	router := mux.NewRouter()
	router.HandleFunc("/", handler.HealthCheck).Methods("GET")
	router.HandleFunc("/ping", handler.HealthCheck).Methods("GET")
	router.HandleFunc("/version", handler.Version).Methods("GET")
	router.HandleFunc("/swagger", handler.Swagger).Methods("GET")
	subroute := router.PathPrefix("/").Subrouter()
	subroute.Use(czel.NewZapMiddleware("router", logger))
	subroute.Use(middleware.JWT(logger))
	//getPokemon with limit
	subroute.HandleFunc("/pokemon/limit", func(w http.ResponseWriter, r *http.Request) {
		fmt.Println("---------------------------------- getPokemonLimit ---------------------------------")
		handler.PokemonLimit(w, r, logger)
	}).Methods("GET")
	//getPokemon by name / ID
	subroute.HandleFunc("/pokemon/{id}", func(w http.ResponseWriter, r *http.Request) {
		fmt.Println("---------------------------------- getPokemon ---------------------------------")
		handler.PokemonData(w, r, logger)
	}).Methods("GET")
	return router
}
