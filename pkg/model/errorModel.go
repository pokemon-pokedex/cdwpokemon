package model

type ErrorResponse struct {
	ErrorMessage string `json:"errorMessage"`
	ResponseCode int    `json:"responseCode"`
}
