package misc

import (
	"bytes"
	"errors"
	"fmt"
	"github.com/nu7hatch/gouuid"
	"gitlab.standartdbank.co.za/cadi/cdwPokemon/pkg/auth"
	"io/ioutil"
	"net/http"
	"os"
)

const (
	xIssuerId      = "x-issuer-id"
	xCorrelationId = "x-correlation-id"
)

func PostCallBackend(payload []byte, url string) ([]byte, error, string, int) {
	client, err := auth.Auth()
	if err != nil {
		message := "certification error"
		return nil, err, message, http.StatusInternalServerError
	}
	req, err := http.NewRequest("POST", url, bytes.NewBuffer(payload))
	if err != nil {
		message := "unable to create http request"
		return nil, err, message, http.StatusInternalServerError
	}
	uuidCreat, err := uuid.NewV4()
	if err != nil {
		message := "error creating uuid"
		return nil, err, message, http.StatusInternalServerError
	}
	uuid := fmt.Sprintf("%v", uuidCreat)
	req.Header.Add("content-type", "application/json")
	req.Header[xIssuerId] = []string{os.Getenv("ISSUERID")}
	req.Header[xCorrelationId] = []string{uuid}

	res, err := client.Do(req)
	if err != nil {
		message := "error executing request"
		return nil, err, message, http.StatusInternalServerError
	}

	respBody, err := ioutil.ReadAll(res.Body)
	if err != nil {
		message := "Failure to read response body"
		return nil, err, message, http.StatusInternalServerError
	}
	err = res.Body.Close()
	if err != nil {
		message := "Failure to close result body"
		return nil, err, message, http.StatusInternalServerError
	}
	if res.StatusCode != 200 {
		fmt.Println(" ")
		fmt.Println("Response payload: ", string(respBody))
		fmt.Println("StatusCode: ",res.StatusCode)
		message := fmt.Sprintf("response failed with %v: %v", res.Status, err)
		err = errors.New("unsuccessful call to service")
		return respBody, err, message, res.StatusCode
	}
	return respBody, nil, "", res.StatusCode

}

func GetCallBackend(url string) ([]byte, error, string, int) {
	method := "GET"

	client := &http.Client {
	}
	req, err := http.NewRequest(method, url, nil)

	if err != nil {
		fmt.Println(err)
		message := "unable to create http request"
		return nil, err, message, http.StatusInternalServerError
	}
	res, err := client.Do(req)
	if err != nil {
		fmt.Println(err)
		message := "unable to call service"
		return nil, err, message, http.StatusInternalServerError
	}
	defer res.Body.Close()

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		fmt.Println(err)
		message := "Failure to read response body"
		return nil, err, message, http.StatusInternalServerError
	}
	//fmt.Println(string(body))
	return body,nil,"",http.StatusOK
}