package misc

import (
	"github.com/stretchr/testify/assert"
	"net/http"
	"testing"
)

func Test_ErrorResp_pass(t *testing.T) {

	ErrorResp("test log message", http.StatusOK)
	assert.Equal(t, 200, 200, "error")

}
