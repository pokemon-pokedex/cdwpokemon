package misc

import (
	"bytes"
	"fmt"
	"github.com/stretchr/testify/mock"
	"io/ioutil"
	"log"
	"net/http"
	"net/http/httptest"
	"testing"
)

func Test_ExtractRequestPayload_should_pass(t *testing.T) {
	ts := httptest.NewServer(
		http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			body, err := ioutil.ReadAll(r.Body)
			if err != nil {
				log.Println("Mock server failed.")
			}
			log.Println("The body:" + string(body))
		}),
	)
	defer ts.Close()
	payload := []byte(
		`{
"pan": "123456789456"
}`)
	r := httptest.NewRequest("POST", ts.URL, bytes.NewBuffer(payload))
	_, err, _, _ := ExtractRequestPayload(r)
	if err != nil {
		t.Fatal("expected success")
	}
}

type mockReadCloser struct {
	mock.Mock
}

func (m *mockReadCloser) Read(p []byte) (n int, err error) {
	args := m.Called(p)
	return args.Int(0), args.Error(1)
}

func (m *mockReadCloser) Close() error {
	args := m.Called()
	return args.Error(0)
}

func Test_ExtractRequestPayload_fail(t *testing.T) {
	mockReadCloser := mockReadCloser{}
	mockReadCloser.On("Read", mock.AnythingOfType("[]uint8")).Return(0, fmt.Errorf("error reading"))
	mockReadCloser.On("Close").Return(fmt.Errorf("error closing"))

	request := &http.Request{
		Body: &mockReadCloser,
	}
	_, err, _, _ := ExtractRequestPayload(request)
	if err == nil {
		t.Fatal("expected failed result")
	}

}
