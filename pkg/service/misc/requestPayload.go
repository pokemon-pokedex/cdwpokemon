package misc

import (
	"fmt"
	"io/ioutil"
	"net/http"
)

func ExtractRequestPayload(r *http.Request) (body []byte, err error, message string, statusCode int) {

	body, err = ioutil.ReadAll(r.Body)
	if err != nil {
		message := fmt.Sprintf("unable to read request payload")
		return nil, err, message, http.StatusInternalServerError
	}

	return body, nil, "", http.StatusOK
}
