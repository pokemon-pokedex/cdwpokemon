package misc

import (
	"go.elastic.co/ecszap"
	"go.uber.org/zap"
	"os"
)

func InitLogger() *zap.Logger {
	encoderConfig := ecszap.NewDefaultEncoderConfig()
	core := ecszap.NewCore(encoderConfig, os.Stdout, zap.DebugLevel)
	logger := zap.New(core, zap.AddCaller())
	return logger
}
