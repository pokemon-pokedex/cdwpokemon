package misc

import (
	"encoding/json"
	"gitlab.standartdbank.co.za/cadi/cdwPokemon/pkg/model"
)

func ErrorResp(errorMessage string, responseCode int) []byte {
	errorMsg := model.ErrorResponse{}
	errorMsg.ErrorMessage = errorMessage
	errorMsg.ResponseCode = responseCode
	responseMessage, _ := json.Marshal(errorMsg)
	return responseMessage

}
