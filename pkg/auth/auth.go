package auth

import (
	"crypto/tls"
	"crypto/x509"
	b64 "encoding/base64"
	"flag"
	"io/ioutil"
	"net/http"
	"os"
)

var (
	caFile = flag.String("CA", "pkg/auth/ca.cer", "A PEM eoncoded CA's certificate file.")
	caFileProd = flag.String("CAPROD", "pkg/auth/caprod.cer", "A PEM eoncoded CA's certificate file.")
)

func Auth() (*http.Client, error) {
	flag.Parse()

	myKey, _ := b64.StdEncoding.DecodeString(os.Getenv("MYKEY"))
	myCert, _ := b64.StdEncoding.DecodeString(os.Getenv("MYCERT"))

	// Load client cert
	cert, err := tls.X509KeyPair(myCert, myKey)
	if err != nil {
		return nil, err
	}

	// Load CA cert
	caCert, err := ioutil.ReadFile(*caFile)
	if os.Getenv("PROD")=="PROD"{
		caCert, err = ioutil.ReadFile(*caFileProd)
	}
	if err != nil {
		return nil, err
	}
	caCertPool := x509.NewCertPool()
	caCertPool.AppendCertsFromPEM(caCert)

	// Setup HTTPS client
	tlsConfig := &tls.Config{
		Certificates: []tls.Certificate{cert},
		RootCAs:      caCertPool,
	}
	tlsConfig.BuildNameToCertificate()
	transport := &http.Transport{TLSClientConfig: tlsConfig}
	httpclient := &http.Client{Transport: transport}

	return httpclient, nil
}
