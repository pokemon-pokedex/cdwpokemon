package middleware

import (
	"go.uber.org/zap"
	"net/http"
)

type token struct {
	logZ *zap.Logger
}

func JWT(logger *zap.Logger) func(next http.Handler) http.Handler {
	return token{
		logZ: logger,
	}.middleware
}

func (c token) middleware(next http.Handler) http.Handler {
	fn := func(w http.ResponseWriter, r *http.Request) {
		//xCorrelationId := r.Header.Get("x-correlation-id")
		//if xCorrelationId == "" {
		//	message := "x-correlation-id missing in header, please add x-correlation-id and try again"
		//	c.logZ.Error(message, zap.Error(errors.New(message)))
		//	errorResp := misc.ErrorResp(message, http.StatusBadRequest)
		//	w.Header().Set("Content-Type", "application/json")
		//	w.WriteHeader(http.StatusBadRequest)
		//	w.Write(errorResp)
		//	return
		//}
		next.ServeHTTP(w, r)
	}
	return http.HandlerFunc(fn)
}
