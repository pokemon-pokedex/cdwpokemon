package handler

import (
	"fmt"
	"github.com/gorilla/mux"
	"gitlab.standartdbank.co.za/cadi/cdwPokemon/pkg/service/misc"
	"go.uber.org/zap"
	"log"
	"net/http"
)

const limit = "100"

func PokemonLimit(w http.ResponseWriter, r *http.Request, logger *zap.Logger) {
	xCorrelationId := r.Header.Get("x-correlation-id")
	w.Header().Set("Content-Type", "application/json")
	params := mux.Vars(r)
	objid := params["id"]
	fmt.Println("Query string key value", objid)
	url := "https://pokeapi.co/api/v2/pokemon?limit=" + limit + "&offset=0"
	body, err, message, statusCode := misc.GetCallBackend(url)
	if err != nil {
		logger.Error(message, zap.Error(err), zap.String("x-correlation-id", xCorrelationId))
		errorResp := misc.ErrorResp(message, statusCode)
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(statusCode)
		w.Write(errorResp)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(body)
}

func PokemonData(w http.ResponseWriter, r *http.Request, logger *zap.Logger) {
	xCorrelationId := r.Header.Get("x-correlation-id")
	w.Header().Set("Content-Type", "application/json")
	params := mux.Vars(r)
	objid := params["id"]
	fmt.Println("Query string key value", objid)
	url := "https://pokeapi.co/api/v2/pokemon/" + objid
	body, err, message, statusCode := misc.GetCallBackend(url)
	if err != nil {
		logger.Error(message, zap.Error(err), zap.String("x-correlation-id", xCorrelationId))
		errorResp := misc.ErrorResp(message, statusCode)
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(statusCode)
		w.Write(errorResp)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(body)
}

func HealthCheck(w http.ResponseWriter, _ *http.Request) {
	w.WriteHeader(http.StatusOK)
	w.Header().Set("Content-Type", "application/json")
	_, err := w.Write([]byte(`{"message":"Server healthy","success":true,"data":null}`))
	if err != nil {
		log.Println(err.Error())
	}
}

func Version(w http.ResponseWriter, r *http.Request) {
	http.ServeFile(w, r, "info.json")
}

func Swagger(w http.ResponseWriter, r *http.Request) {

	http.ServeFile(w, r, "swagger.json")
}
