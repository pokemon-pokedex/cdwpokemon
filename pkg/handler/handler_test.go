package handler

import (
	"bytes"
	"github.com/stretchr/testify/assert"
	"gitlab.standartdbank.co.za/cadi/cdwPokemon/pkg/service/misc"
	"io/ioutil"
	"log"
	"net/http"
	"net/http/httptest"
	"testing"
)

var logger = misc.InitLogger()

func Test_UpdateCard_fail_400(t *testing.T) {
	ts := httptest.NewServer(
		http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			body, err := ioutil.ReadAll(r.Body)
			if err != nil {
				log.Println("Mock server failed.")
			}
			log.Println("The body:" + string(body))
		}),
	)
	payload := []byte(
		`{
  "issuerCardRefId": "",
  "newIssuerCardRefId": "string",
  "fpanDetails": {
      "newExpDate":"date",
      "newFPAN":"string",
      "newPSN":"string",
      "oldExpDate":"date",
      "oldFPAN":"string",
      "oldPSN":"string"
  },
  "publicKeyIdentifier": "string",
  "newProductId": "string",
  "metadata": {
    "cardIssuer": "string",
    "foregroundColor": "string",
    "backgroundColor": "string",
    "labelColor": "string",
    "shortDescription": "string",
    "longDescription": "string",
    "contactWebsite": "string",
    "contactName": "string",
    "contactEmail": "string@ss.com",
    "contactNumber": "string",
    "privacyPolicyURL": "string",
    "termsAndConditionsURL": "string",
    "supportsTokenNotifications": "string",
    "supportsFPANNotifications": "string",
    "appLaunchURL": "string",
    "appLaunchURLScheme": "string",
    "transactionServiceURL": "string",
    "transactionPushTopic": "string",
    "messageServiceURL": "string",
    "messagePushTopic": "string"
  },
  "newCardMetadata": {
    "cardholderName": "string",
    "fpanLastDigits": "string",
    "fpanBin": "string",
    "fpanExpiryDate": "string"
  }
}`)
	w := httptest.NewRecorder()
	r := httptest.NewRequest("POST", ts.URL, bytes.NewBuffer(payload))
	UpdateCard(w, r, logger)
	assert.Equal(t, 400, w.Code, "error")

}

func Test_UpdateCard_fail_500(t *testing.T) {
	ts := httptest.NewServer(
		http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			body, err := ioutil.ReadAll(r.Body)
			if err != nil {
				log.Println("Mock server failed.")
			}
			log.Println("The body:" + string(body))
		}),
	)
	payload := []byte(
		`{
  "issuerCardRefId": "afafa",
  "newIssuerCardRefId": "string",
  "fpanDetails": {
      "newExpDate":"date",
      "newFPAN":"string",
      "newPSN":"string",
      "oldExpDate":"date",
      "oldFPAN":"string",
      "oldPSN":"string"
  },
  "publicKeyIdentifier": "string",
  "newProductId": "string",
  "metadata": {
    "cardIssuer": "string",
    "foregroundColor": "string",
    "backgroundColor": "string",
    "labelColor": "string",
    "shortDescription": "string",
    "longDescription": "string",
    "contactWebsite": "string",
    "contactName": "string",
    "contactEmail": "string@ss.com",
    "contactNumber": "string",
    "privacyPolicyURL": "string",
    "termsAndConditionsURL": "string",
    "supportsTokenNotifications": "string",
    "supportsFPANNotifications": "string",
    "appLaunchURL": "string",
    "appLaunchURLScheme": "string",
    "transactionServiceURL": "string",
    "transactionPushTopic": "string",
    "messageServiceURL": "string",
    "messagePushTopic": "string"
  },
  "newCardMetadata": {
    "cardholderName": "string",
    "fpanLastDigits": "string",
    "fpanBin": "string",
    "fpanExpiryDate": "string"
  }
}`)
	w := httptest.NewRecorder()
	r := httptest.NewRequest("POST", ts.URL, bytes.NewBuffer(payload))
	UpdateCard(w, r, logger)
	assert.Equal(t, 500, w.Code, "error")
}
