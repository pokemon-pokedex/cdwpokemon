module gitlab.standartdbank.co.za/cadi/cdwPokemon

go 1.16

require (
	github.com/asaskevich/govalidator v0.0.0-20210307081110-f21760c49a8d // indirect
	github.com/go-chi/chi v4.1.2+incompatible // indirect
	github.com/gorilla/mux v1.8.0
	github.com/igknot/chi-zap-ecs-logger v0.0.0-20210521132916-301d52e01749
	github.com/nu7hatch/gouuid v0.0.0-20131221200532-179d4d0c4d8d
	github.com/rs/cors v1.9.0
	github.com/stretchr/testify v1.7.0
	go.elastic.co/ecszap v1.0.0
	go.uber.org/zap v1.17.0
)
